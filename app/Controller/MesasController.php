<?php
    class MesasController extends AppController{
        public $helpers = array('Html', 'Form', 'Time');
        public $components = array('Flash');

        public function index(){
            $this->set('mesas', $this->Mesa->find('all')); #Injectando os dados do objeto Mesa na var mesas
        }

        public function nuevo(){
            if ($this->request->is('post')) {
                $this->Mesa->create();
                if ($this->Mesa->save($this->request->data)) {
                    $this->Flash->success('La mesa ha sido creada.');
                    return $this->redirect(array('action' => 'index'));
                }
                $this->Flash->set('No se pudo crear la Mesa!');
            }

            $meseros = $this->Mesa->Mesero->find('list', array('fields' => array('id', 'nombre_completo'))); // recebe a lista de meseros + campo virtual (virtualfields) com o nome completo (mesero.php:4)
            $this->set('meseros', $meseros); #model>Mesa.php:4
        }

    }