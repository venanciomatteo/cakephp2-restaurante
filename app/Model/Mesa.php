<?php
    class Mesa extends AppModel{
        public $belongsTo = array( #Mesa pertence à um Mesero
            'Mesero' => array(
                'className'     => 'Mesero',
                'foreignKey'    => 'mesero_id',
            )
        );

        public $validate = array(
            'serie' => array(
                'notBlank'  => array('rule' => 'notBlank'),
                'numeric'   => array(
                    'rule'      => 'numeric',
                    'message'   => 'Solo números',
                ),
                'unique'    =>  array(
                    'rule'  => 'isUnique', 
                    'message'   => 'El Serie de Mesa ya se encuentra en nuestra base de datos'
                )
            ),
            'puestos' => array(
                'notBlank'  => array('rule' => 'notBlank'),
                'numeric'   => array(
                    'rule'      => 'numeric',
                    'message'   => 'Solo números',
                ),
                'unique'    =>  array(
                    'rule'  => 'isUnique', 
                    'message'   => 'El Puesto ya se encuentra en nuestra base de datos'
                )
            ),
            'posicion' => array(
                'notBlank'  => array('rule' => 'notBlank'),
            ),
        );
    }