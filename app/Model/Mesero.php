<?php
    class Mesero extends AppModel{
        public $virtualFields = array(
            'nombre_completo' => 'CONCAT(Mesero.nombre, " ", Mesero.apellido)'
        );

        public $validate = array(
            'dni'       => array(
                'notBlank'  => array('rule' => 'notBlank'),
                'numeric'   => array(
                    'rule'      => 'numeric',
                    'message'   => 'Solo números',
                ),
                'unique'    =>  array(
                    'rule'  => 'isUnique', 
                    'message'   => 'El DNI ya se encuentra en nuestra base de datos'
                )
             ),
            'nombre'    => array('rule' => 'notBlank' ),
            'apellido'  => array('rule' => 'notBlank' ),
            'telefono'       => array(
                'notBlank'      => array('rule' => 'notBlank'),
                'numeric'       => array(
                    'rule'          => 'numeric',
                    'message'       => 'Solo números'
                )
             ),
        );
        # Relacionamentos são sempre definidos nos Models
        # Mesero tem várias 'Mesas'
        # o $hasMany irá trazer um array com as mesas associadas ao Mesero
        public $hasMany = array( 
            'Mesa' => array( // Elemento 'Mesa' a ser recuperado na Vista
                'className'     => 'Mesa',
                'foreignKey'    => 'mesero_id',
                'conditions'    => '',
                'order'         => 'Mesa.serie DESC',
                'depend'        => false // delete cascade
            )
        );
    }