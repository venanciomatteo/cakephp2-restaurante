<h2>Lista de Mesas</h2>
<p><?php echo $this->Html->link('Crear Nova Mesa', array('controller' => 'mesas', 'action' => 'nuevo')); ?> </p>

<table>
    <td>Serie</td>
    <td>Puestos</td>
    <td>Posición</td>
    <td>Responsable</td>
    <td><strong>Creado: </strong> </td>
    <td><strong>Modificado: </strong></td>
    <td>Acciones</td>

    <?php 
    #MesasController.php:7
    foreach($mesas as $mesa): ?> 
    <tr>
        <td> <?php echo $mesa['Mesa']['serie']; ?> </td>
        <td> <?php echo $mesa['Mesa']['puestos']; ?> </td>
        <td> <?php echo $mesa['Mesa']['posicion']; ?> </td>
        <td> <?php echo $this->Html->link($mesa['Mesero']['nombre'] .' '. $mesa['Mesero']['apellido'], array('controller' => 'meseros', 'action' => 'ver', $mesa['Mesero']['id'])) ?> </td>
        <td> <?php echo $this->Time->format('d-m-Y - h:i A',  $mesa['Mesa']['created'] ); ?> </td>
        <td> <?php if (!empty($mesero['Mesero']['modified'])){
                        echo $this->Time->format('d-m-Y - h:i A',  $mesa['Mesa']['modified'] ); } else { echo ' - '; } 
             ?> </td>
        <td> 
            <?php echo $this->Html->link('Detalles', array('controller' => 'mesas', 'action' => 'ver', $mesa['Mesa']['id'])); ?> 
            <?php echo $this->Html->link('Editar', array('controller' => 'mesas', 'action' => 'editar', $mesa['Mesa']['id'])); ?> 
            <?php echo $this->Form->postLink('Eliminar', array('action' => 'eliminar', $mesa['Mesa']['id']), array('confirm' => 'Eliminar a mesa '.$mesa['Mesa']['serie'].'?')); ?> 
        </td>
        </tr>
    <?php endforeach; ?>

</table>