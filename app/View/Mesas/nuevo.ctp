<h2>Crear Mesa</h2>
<?php
    echo $this->Form->create('Mesa'); // Criando um novo Formulário Mesa:Post

    echo $this->Form->input('serie');
    echo $this->Form->input('puestos');
    echo $this->Form->input('posicion', array('rows' => 3));
    echo $this->Form->input('mesero_id'); // Lista carregada no Controlador MesasController
    echo $this->Form->end('Crear Mesa');

    echo $this->Html->link('Volver a lista de mesas', array('controller' => 'mesas', 'action' => 'index'));