<h2>Lista de Meseros</h2>
<p><?php echo $this->Html->link('Crear Mesero', array('controller' => 'meseros', 'action' => 'nuevo')); ?> </p>

<table>
    <td>Id</td>
    <td>Nombre</td>
    <td>Apellido</td>
    <td>Acciones</td>

    <?php 
    foreach($meseros as $mesero): ?>
    <tr>
        <td> <?php echo $mesero['Mesero']['id']; ?> </td>
        <td> <?php echo $mesero['Mesero']['nombre']; ?> </td>
        <td> <?php echo $mesero['Mesero']['apellido']; ?> </td>
        <td> 
            <?php echo $this->Html->link('Detalles', array('controller' => 'meseros', 'action' => 'ver', $mesero['Mesero']['id'])); ?> 
            <?php echo $this->Html->link('Editar', array('controller' => 'meseros', 'action' => 'editar', $mesero['Mesero']['id'])); ?> 
            <?php echo $this->Form->postLink('Eliminar', array('action' => 'eliminar', $mesero['Mesero']['id']), array('confirm' => 'Eliminar a '.$mesero['Mesero']['nombre'].'?')); ?> 
        </td>
        </tr>
    <?php endforeach; ?>

</table>