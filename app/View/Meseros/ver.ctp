<h2>Detalle del Meser@ <?php echo $mesero['Mesero']['nombre']; ?></h2>

<p><strong>DNI: </strong> <?php echo $mesero['Mesero']['dni']; ?></p>
<p><strong>Teléfono: </strong> <?php echo $mesero['Mesero']['telefono']; ?></p>
<p><strong>Creado: </strong> <?php echo $this->Time->format('d-m-Y - h:i A',  $mesero['Mesero']['created'] ); ?></p>
<?php if (!empty($mesero['Mesero']['modified'])){ ?>
<p><strong>Modificado: </strong> <?php echo $this->Time->format('d-m-Y - h:i A', $mesero['Mesero']['modified']); ?></p>
<?php } 
echo $this->Html->link('Volver a lista de meseros', array('controller' => 'meseros', 'action' => 'index'));
?>

<pre>
<?php
    print_r($mesero['Mesa']);
?>
</pre>

<?php if ( sizeof($mesero['Mesa']) > 1 ) { ?>
    <h3>Encargado de las mesas: </h3>
<?php } else { ?>
    <h3>Encargado de la mesa: </h3>
<?php } ?> 

<?php if (empty($mesero['Mesa'])) : ?>
      <p>No tiene mesas asociadas</p>
<?php endif ?>  

<?php foreach($mesero['Mesa'] as $m) : ?>
    <p><strong>Serie: </strong> <?php echo $m['serie']; ?></p>
    <p><strong>Puestos: </strong> <?php echo $m['puestos']; ?></p>
    <p><strong>Posición: </strong> <?php echo $m['posicion']; ?></p>
    <p><strong>Creado: </strong> <?php echo $this->Time->format('d-m-Y - h:i A',  $m['created'] ); ?></p>
    <p><hr></p>
<?php endforeach; ?>